﻿'Cerrar Sesion
'Date: 4/05/2021
'Tester

Set objBrowser = Browser("name:=Mercado Libre México")
Set objPage = objBrowser.Page("title:=Mercado Libre México")

Set linkUsuarioActivo = objPage.Link("class:=nav-header-user-myml", "html tag:=A", "index:=0")
Set linkSalir = objPage.Link("name:=Salir", "index:=0")
Set linkIngresa = objPage.Link("name:=Ingresa", "html tag:=A", "index:=0")

linkUsuarioActivo.HoverTap
wait 1
linkSalir.Click

If linkIngresa.Exist(5) Then
	fnEvidenciaPasada "Cerrar sesion exitoso", "Se cierra la sesion de manera exitosa"
Else
	fnEvidenciaFallada "Cerrar sesion NO exitoso", "NO se cierra la sesion de manera exitosa"
End If

objBrowser.CloseAllTabs()
