﻿
'Subir Publicacion de telefono a ML tipo de moneda Pesos
'Precondiciones: ninguna
'Fecha: 4/05/2021
'Tester: FSA

'On error resume next
'	strID = DataTable.Value("ID")
'	strEjecucion = Ucase(DataTable.Value("EJECUCION"))
'	
'	If err.number = -2147220907 Then
'		print("Hubo un error en la primera fila")
'	End If
	
'strHojaExcel = "Execution"
'strHojaUFT = "global"
'DataTable.ImportSheet  "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\ML\Data\Execution.xlsx", strHojaExcel, strHojaUFT
'DataTable.Value("ESTADO", dtglobalsheet) = "Valor a emupjar en la tabla"

strEjecucion = Ucase(DataTable.Value("EJECUCION"))
strID = DataTable.Value("ID")

If strEjecucion = oEjecucion.YES And strID = oTestCaseID.TC_ML_PUB_MX Then

	RunAction "Iniciar_Sesion [IniciarSesionML]", oneIteration
	
	RunAction "Subir_Publicacion [SubirPublicacion]", oneIteration, oTestCaseID.TC_ML_PUB_MX
	
	RunAction "Cerrar_Sesion [CerrarSesion]", oneIteration

ElseIf strEjecucion = oEjecucion.YES And strID = oTestCaseID.TC_ML_PUB_USD Then

	RunAction "Iniciar_Sesion [IniciarSesionML]", oneIteration
	
	RunAction "Subir_Publicacion [SubirPublicacion]", oneIteration, oTestCaseID.TC_ML_PUB_TC_ML_PUB_USD
	
	RunAction "Cerrar_Sesion [CerrarSesion]", oneIteration
End If

'DataTable.ExportSheet "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\ML\Data\Execution.xlsx", strHojaUFT, strHojaExcel
