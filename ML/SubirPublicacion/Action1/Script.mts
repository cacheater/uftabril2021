﻿'Sube una publicacion en ML
'Preciondicion: Usuario con sesion iniciada
'Fecha: 28/04/2021
'Tester: Fabian

'Declaracion de objetos del repositorio
Set objBrowserML = Browser("name:=Mercado Libre México")
Set objPageML = objBrowserML.Page("title:=Mercado Libre México")

Set objBrowserQuePublicar = Browser("name:=Elige qué publicar")
Set objPageQuePublicar = objBrowserQuePublicar.Page("title:=Elige qué publicar")

Set objBrowserPublicar = Browser("name:=Publicar")
Set objPagePublicar = objBrowserPublicar.Page("title:=Publicar")

Set linkVender = objPageML.Link("name:=Vender", "index:=0")
Set elementProductos = objPageQuePublicar.WebElement("html tag:=SPAN", "innertext:=Productos", "index:=0")

Set buttonEmpezarPublicar = objPagePublicar.WebButton("name:=Empezar a publicar")
Set editTituloPublicacion = objPagePublicar.WebEdit("html id:=products_finder_input")
Set buttonComenzar = objPagePublicar.WebButton("name:=Comenzar")
Set elementCategoria = objPagePublicar.WebElement("html id:=MLM1055")
Set editMarca = objPagePublicar.WebEdit("placeholder:=Escribe Marca")
Set elementCarSim = objPagePublicar.WebElement("html tag:=DIV", "innertext:=Es Dual SIM", "index:=0")
Set elementFlechaMarca = objPagePublicar.WebElement("html tag:=DIV", "class:=sc-ui-icon sc.*", "index:=1") 
Set editModelo = objPagePublicar.WebEdit("placeholder:=Escribe Modelo", "html tag:=INPUT")
Set elementFlechaModelo = objPagePublicar.WebElement("html tag:=DIV", "class:=sc-ui-icon sc.*", "index:=1") '-ui-chevron--down")
Set elementCapacidad = objPagePublicar.WebElement("html tag:=LI", "innertext:=64 GB")
Set elementMemoriaRam = objPagePublicar.WebElement("html tag:=LI", "innertext:=4 GB")
Set elementFlechaColor = objPagePublicar.WebElement("html tag:=DIV", "class:=sc-ui-icon sc.*", "index:=1")
Set elementIdentificandoProducto = objPagePublicar.WebElement("html tag:=H2", "innertext:=Empecemos identificando tu producto")
Set buttonConfirmar = objPagePublicar.WebButton("name:=Confirmar", "index:=0")
Set elementEliminarPublicacion = objPageQuePublicar.WebElement("class:=sc-ui-icon", "html tag:=svg", "index:=0")
Set elementCompania = objPagePublicar.WebElement("html tag:=LI", "innertext:=Desbloqueado")
Set elementNuevo = objPagePublicar.WebElement("html id:=NEW")
Set buttonConfirmarPublicacion = objPagePublicar.WebButton("name:=Confirmar", "index:=1")
Set editCodigoUniversal = objPagePublicar.WebEdit("html tag:=INPUT", "class:=andes-form-control__field")
Set elementNoLoTengoAhora = objPagePublicar.WebElement("class:=andes-checkbox__mimic", "html tag:=SPAN", "index:=1")
Set buttonContinuar = objPagePublicar.WebButton("name:=Continuar", "index:=0")
Set editCantidad = objPagePublicar.WebEdit("html id:=quantity")
Set buttonConfirmarCantidad = objPagePublicar.WebButton("name:=Confirmar", "index:=2")
Set buttonSiguiente = objPagePublicar.WebButton("name:=Siguiente", "index:=0")
Set editPrecio = objPagePublicar.WebEdit("class:=andes-form-control__field", "html tag:=INPUT")
Set buttonConfirmarPrecio = objPagePublicar.WebButton("name:=Confirmar", "index:=3")
Set elementElegirClasica = objPagePublicar.WebElement("html tag:=SPAN", "innertext:=Elegir Clásica")
Set elementSinGarantia = objPagePublicar.WebElement("html tag:=SPAN", "innertext:=Sin garantía")
Set buttonPublicar = objPagePublicar.WebButton("name:=Publicar")
Set linkLogo = objPagePublicar.Link("class:=nav-logo", "name:=Mercado Libre México - Donde comprar y vender de todo")

Set buttonPesos = objPagePublicar.WebButton("xpath:=//button[@tabindex='-1']")
Set elementDolar = objPagePublicar.WebElement("class:=andes-list__item")
Set elementListo = objPagePublicar.WebElement("html tag:=H2", "innertext:=¡Listo!")

'Declaracion de Variables globales
'strHojaExcel = "Data"
'strHojaUFT = "global"
'DataTable.ImportSheet  "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\ML\Data\Execution.xlsx", strHojaExcel, strHojaUFT
strTituloProducto = DataTable.Value("TITULO_PRODUCTO")
strMarca = DataTable.Value("MARCA")
strModelo = DataTable.Value("MODELO")
strCantidad = DataTable.Value("CANTIDAD")
strPrecio = fnRandomNumber(6)
strID = Parameter("ID") 

'Bifurcacion de acuerdo al parametor de entrada
'ID Caso de pruebas
If strID = oTestCaseID.TC_ML_PUB_MX Then
	Name = oTestCaseName.PUBLICACION_PESOS
ElseIf strID = oTestCaseID.TC_ML_PUB_USD Then
	Name = oTestCaseName.PUBLICACION_DOLARES
End If

'Flujo del Script
linkVender.Click

If elementEliminarPublicacion.Exist(4) Then
	elementEliminarPublicacion.Click
End If

elementProductos.Click

'buttonEmpezarPublicar.Click
editTituloPublicacion.Set strTituloProducto
buttonComenzar.Click
elementCategoria.Click
editMarca.Set strMarca
elementFlechaMarca.Click
elementCarSim.Click
editModelo.Set strModelo
elementFlechaModelo.Click
elementCapacidad.Click
elementMemoriaRam.Click
elementFlechaColor.Click

If elementIdentificandoProducto.Exist(2) Then
	'Tomar una captura de pantalla
End If

buttonConfirmar.Click
elementCompania.Click
elementNuevo.Click
buttonConfirmarPublicacion.Click

'editCodigoUniversal.Set strCodigoUniversal
elementNoLoTengoAhora.Click
buttonContinuar.Click
editCantidad.Set strCantidad
buttonConfirmarCantidad.Click
buttonSiguiente.Click
editPrecio.Set strPrecio

If strID = oTestCaseID.TC_ML_PUB_USD Then
	buttonPesos.Click
	elementDolar.Click
End If

buttonConfirmarPrecio.Click
elementElegirClasica.Click
buttonConfirmarPublicacion.Click
buttonConfirmarCantidad.Click
buttonContinuar.Click
elementSinGarantia.Click
buttonConfirmarPrecio.Click
buttonPublicar.Click

If elementListo.Exist(7) Then
	fnEvidenciaPasada "Evidencia de publicacion de producto", "Publicacion punto a punto de forma correcta"
Else
	fnEvidenciaFallada  "Evidencia de publicacion de producto", "Publicacion punto a punto de forma incorrecta"
End If
	
linkLogo.Click
