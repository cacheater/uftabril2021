﻿
'Subir Publicacion de telefono a ML tipo de moneda Dolares
'Precondiciones: ninguna
'Fecha: 4/05/2021
'Tester: FSA

On error resume next
	strID = DataTable.Value("ID")
	strEjecucion = Ucase(DataTable.Value("EJECUCION"))
	
	If err.number = -2147220907 Then
		print("Hubo un error en la primera fila")
	End If
	
strHojaExcel = "Execution"
strHojaUFT = "global"
DataTable.ImportSheet  "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\ML\Data\Execution.xlsx", strHojaExcel, strHojaUFT


If strEjecucion = oEjecucion.YES And strID = oTestCaseID.TC_ML_PUB_USD Then
	
	RunAction "Iniciar_Sesion [IniciarSesionML]", oneIteration
	
	RunAction "Subir_Publicacion [SubirPublicacion]", oneIteration, oTestCaseID.TC_ML_PUB_USD
	
	RunAction "Cerrar_Sesion [CerrarSesion]", oneIteration

Else
	'No haces nada
End  If
