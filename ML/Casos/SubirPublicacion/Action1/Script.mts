﻿'Sube una publicacion en ML
'Preciondicion: Usuario con sesion iniciada
'Fecha: 28/04/2021
'Tester: Fabian

'Declaracion de objetos del repositorio
Set objBrowserML = Browser("name:=Mercado Libre México")
Set objPageML = objBrowserML.Page("title:=Mercado Libre México")

Set objBrowserQuePublicar = Browser("name:=Elige qué publicar")
Set objPageQuePublicar = objBrowserQuePublicar.Page("title:=Elige qué publicar")

Set objBrowserPublicar = Browser("name:=Publicar")
Set objPagePublicar = objBrowserPublicar.Page("title:=Publicar")

Set linkVender = objPageML.Link("name:=Vender", "index:=0")
Set elementProductos = objPageQuePublicar.WebElement("html tag:=SPAN", "innertext:=Productos", "index:=0")

Set buttonEmpezarPublicar = objPagePublicar.WebButton("name:=Empezar a publicar")
Set editTituloPublicacion = objPagePublicar.WebEdit("html id:=products_finder_input")
Set buttonComenzar = objPagePublicar.WebButton("name:=Comenzar")
Set elementCategoria = objPagePublicar.WebElement("html id:=MLM1055")
Set editMarca = objPagePublicar.WebEdit("placeholder:=Escribe Marca")
Set elementCarSim = objPagePublicar.WebElement("html tag:=DIV", "innertext:=Es Dual SIM", "index:=0")
Set elementFlechaMarca = objPagePublicar.WebElement("html tag:=DIV", "class:=sc-ui-icon sc.*", "index:=1") 
Set editModelo = objPagePublicar.WebEdit("placeholder:=Escribe Modelo", "html tag:=INPUT")
Set elementFlechaModelo = objPagePublicar.WebElement("html tag:=DIV", "class:=sc-ui-icon sc.*", "index:=1") '-ui-chevron--down")
Set elementCapacidad = objPagePublicar.WebElement("html tag:=LI", "innertext:=64 GB")
Set elementMemoriaRam = objPagePublicar.WebElement("html tag:=LI", "innertext:=4 GB")
Set elementFlechaColor = objPagePublicar.WebElement("html tag:=DIV", "class:=sc-ui-icon sc.*", "index:=1")
Set elementIdentificandoProducto = objPagePublicar.WebElement("html tag:=H2", "innertext:=Empecemos identificando tu producto")
Set buttonConfirmar = objPagePublicar.WebButton("name:=Confirmar", "index:=0")
Set elementEliminarPublicacion = objPageQuePublicar.WebElement("class:=sc-ui-icon", "html tag:=svg", "index:=0")
Set elementCompania = objPagePublicar.WebElement("html tag:=LI", "innertext:=Desbloqueado")
Set elementNuevo = objPagePublicar.WebElement("html id:=NEW")
Set buttonConfirmarPublicacion = objPagePublicar.WebButton("name:=Confirmar", "index:=1")
Set editCodigoUniversal = objPagePublicar.WebEdit("html tag:=INPUT", "class:=andes-form-control__field")
Set elementNoLoTengoAhora = objPagePublicar.WebElement("class:=andes-checkbox__mimic", "html tag:=SPAN", "index:=1")
Set buttonContinuar = objPagePublicar.WebButton("name:=Continuar", "index:=0")
Set editCantidad = objPagePublicar.WebEdit("html id:=quantity")
Set buttonConfirmarCantidad = objPagePublicar.WebButton("name:=Confirmar", "index:=2")
Set buttonSiguiente = objPagePublicar.WebButton("name:=Siguiente", "index:=0")
Set editPrecio = objPagePublicar.WebEdit("class:=andes-form-control__field", "html tag:=INPUT")
Set buttonConfirmarPrecio = objPagePublicar.WebButton("name:=Confirmar", "index:=3")
Set elementElegirClasica = objPagePublicar.WebElement("html tag:=SPAN", "innertext:=Elegir Clásica")
Set elementSinGarantia = objPagePublicar.WebElement("html tag:=SPAN", "innertext:=Sin garantía")
Set buttonPublicar = objPagePublicar.WebButton("name:=Publicar")
Set linkLogo = objPagePublicar.Link("class:=nav-logo", "name:=Mercado Libre México - Donde comprar y vender de todo")

Set buttonPesos = objPagePublicar.WebButton("xpath:=//button[@tabindex='-1']")
Set elementDolar = objPagePublicar.WebElement("class:=andes-list__item")

'elementDolar.Click
'wait 1

'Declaracion de Variables globales
strTituloProducto = "Celular"
strMarca = "Huawei"
strModelo = "P30 Lite"
strCantidad = "1"
strPrecio = fnRandomNumber(4)
strID = Parameter("ID") 
'strCodigoUniversal = fnRandomNumber(14)

'Bifurcacion de acuerdo al parametor de entrada
'ID Caso de pruebas
If strID = "TC_ML_PUB_MX" Then
	Name = "Publicacion en pesos"
ElseIf strID = "TC_ML_PUB_USD"  Then
	Name = "Publicacion en USD"
End If

'Flujo del Script
linkVender.Click

If elementEliminarPublicacion.Exist(4) Then
	elementEliminarPublicacion.Click
End If

elementProductos.Click

'buttonEmpezarPublicar.Click
editTituloPublicacion.Set strTituloProducto
buttonComenzar.Click
elementCategoria.Click
editMarca.Set strMarca
elementFlechaMarca.Click
elementCarSim.Click
wait 2
editModelo.Set strModelo
wait 2
elementFlechaModelo.Click
wait 3
elementCapacidad.Click
wait 2
elementMemoriaRam.Click
wait 2
elementFlechaColor.Click

If elementIdentificandoProducto.Exist(2) Then
	'Tomar una captura de pantalla
End If

buttonConfirmar.Click
wait 2
elementCompania.Click
wait 2
elementNuevo.Click
wait 2
buttonConfirmarPublicacion.Click
wait 2

'editCodigoUniversal.Set strCodigoUniversal
elementNoLoTengoAhora.Click
wait 2
buttonContinuar.Click
wait 2
editCantidad.Set strCantidad
wait 2
buttonConfirmarCantidad.Click
wait 2
buttonSiguiente.Click
wait 2
editPrecio.Set strPrecio

If strID = "TC_ML_PUB_USD"  Then
	buttonPesos.Click
	elementDolar.Click
End If

wait 2
buttonConfirmarPrecio.RefreshObject()
buttonConfirmarPrecio.Click
wait 2
elementElegirClasica.Click
wait 2
buttonConfirmarPublicacion.RefreshObject()
buttonConfirmarPublicacion.Click
wait 2
buttonConfirmarCantidad.RefreshObject()
buttonConfirmarCantidad.Click
wait 2
buttonContinuar.RefreshObject()
buttonContinuar.Click
wait 2
elementSinGarantia.Click
wait 2
buttonConfirmarPrecio.RefreshObject()
buttonConfirmarPrecio.Click
wait 2
'buttonPublicar.Click

fnEvidenciaPasada "Evidencia de publicacion de producto", "Publicacion punto a punto de forma correcta"

linkLogo.Click
