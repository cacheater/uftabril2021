﻿
'Subir Publicacion de telefono a ML tipo de moneda Pesos
'Precondiciones: ninguna
'Fecha: 4/05/2021
'Tester: FSA


RunAction "Iniciar_Sesion [IniciarSesionML]", oneIteration

RunAction "Subir_Publicacion [SubirPublicacion]", oneIteration, "TC_ML_PUB_USD"

RunAction "Cerrar_Sesion [CerrarSesion]", oneIteration

