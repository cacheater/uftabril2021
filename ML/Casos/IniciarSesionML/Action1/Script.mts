﻿'Inicia sesion en ML con credenciales correctas
'Fecha: 28/04/2021
'Tester: Fabian
'URL: https://www.mercadolibre.com/jms/mlm/lgz/login?platform_id=ML&go=https%3A%2F%2Fwww.mercadolibre.com.mx%2F&loginType=explicit#nav-header

'Declaracion de objetos del repositorio
Set objBrowserIniciarSesion = Browser("¡Hola, ultimo usuario")
Set objPageIniciarSesion = objBrowserIniciarSesion.Page("¡Hola, ultimo usuario")

Set elementCerrarUsuario = objPageIniciarSesion.WebElement("CerrarUsuario")

Set editUsuario = objPageIniciarSesion.WebEdit("usuario")
Set elementContinuar = objPageIniciarSesion.WebElement("Continuar")

Set editPassword = objPageIniciarSesion.WebEdit("password")
Set buttonIngresar = objPageIniciarSesion.WebButton("Ingresar")
Set linkUsuarioFirmado = objPageIniciarSesion.Link("UsuarioFirmado")


'Declaracion de variables globales
URL = "https://www.mercadolibre.com/jms/mlm/lgz/login?platform_id=ML&go=https%3A%2F%2Fwww.mercadolibre.com.mx%2F&loginType=explicit#nav-header"
strBrowser = DataTable.Value("BROWSER")
strUsuario = DataTable.Value("USUARIO")
strPassword = DataTable.Value("PASSWORD")


'Flujo del script
fnAbrirNavegador strBrowser, URL

If elementCerrarUsuario.Exist(4) Then
	elementCerrarUsuario.Click
End If

wait 2
editUsuario.Set strUsuario
elementContinuar.Click

editPassword.SetSecure  strTuContrasenia @@ script infofile_;_ZIP::ssf1.xml_;_
buttonIngresar.Click @@ script infofile_;_ZIP::ssf2.xml_;_
wait 5

'Expected
If linkUsuarioFirmado.Exist(7) Then
	fnEvidenciaPasada "Usuario FIrmado", "Evidencia usuario firmado correctamente"

Else
	fnEvidenciaFallada "Usuario FIrmado", "El usuario no se firmo de forma correcta"
End If
