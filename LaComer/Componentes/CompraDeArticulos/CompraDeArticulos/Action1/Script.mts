﻿'Component Description:
'Comprar Articulos
'Preconditions: To have started a session
'Tester: Fabian Solano Amador
'Creation Date: 24-February-2020
'http://lacomer.com.mx/lacomer/doPresentaLogin.action?succFmt=100&succld=148&key=LaComer-Lomas-An%C3%A1huac&pago=

''''''''''''''''''''''''''''''''''''''''''''''Object Repository Declaration''''''''''''''''''''''''''''''''''''''''''''''''''''''
Set objBrowserLogInLaComer = Browser("name:=La Comer.*")
Set objPageLogInLaComer = objBrowserLogInLaComer.Page("title:=La Comer.*")

Set txtBusquedaDeProductos = objPageLogInLaComer.WebEdit("html id:=idSearch")
Set bttnBusqueda = objPageLogInLaComer.WebButton("html id:=btnSearch")

Set bttnAgregarAlCarrito = objPageLogInLaComer.WebButton("html id:=btn_addtoCart_.*", "index:=0")
Set imgCarrito = objPageLogInLaComer.Image("html id:=carritoH")
Set txtPzaCarrito = objPageLogInLaComer.WebEdit("html id:=cantpz_.*", "index:=0")
Set bttnX = objPageLogInLaComer.WebButton("acc_name:=Close", "name:=x", "index:=0")

Set bttnEntregaAlDomicilio = objPageLogInLaComer.WebButton("name:=Entrega a domicilio", "html tag:=BUTTON")
Set bttnRecogerEnTienda = objPageLogInLaComer.WebButton("name:=Recoger en tienda", "html tag:=BUTTON")
Set imgArticulo1 = objPageLogInLaComer.Image("class:=li_prod_mosaic", "html tag:=IMG", "name:=Image", "index:=0")
Set imgArticuloCarrito = objPageLogInLaComer.Image("class:=li_prod_list", "html tag:=IMG", "name:=Image", "index:=0")

Set bttnContinuar = objPageLogInLaComer.WebButton("html tag:=BUTTON", "name:=Continuar", "index:=0")
Set wbeGratis = objPageLogInLaComer.WebElement("html tag:=DIV", "innertext:=GRATIS", "index:=2")
Set bttnAceptar = objPageLogInLaComer.WebButton("name:=ACEPTAR", "html tag:=BUTTON", "index:=0", "class:=btn btn-primary pl-30 pr-30")
Set wbeEnLinea = objPageLogInLaComer.WebElement("html tag:=DIV", "class:=btn_circle_img", "index:=0")
Set wbeAmex = objPageLogInLaComer.WebElement("html tag:=SPAN", "class:=amex", "index:=0")

Set imgLogo = objPageLogInLaComer.Image("xpath:=//DIV[@id="&chr(34)& "logo" &chr(34)& "]/A[1]/IMG[1]")
Set wbeEliminarDelCarrito = objPageLogInLaComer.WebElement("class:=glyphicon glyphicon-trash", "html tag:=A", "index:=0")
'wbeEliminarDelCarrito.Highlight
'fnInicializarDataTable("\\vmware-host\Shared Folders\Documents\Unified Functional Testing\LaComer\Data\LaComerDataTable.xlsx")
'DataTable.SetCurrentRow 6
''''''''''''''''''''''''''''''''''''''''''''''Global Variables Declaration'''''''''''''''''''''''''''''''''''''''''''''''''''''''
Const STR_NAME = "objName"
strTestCaseId = Parameter("TEST_CASE_ID")
strProductoParaBusqueda = fnGetProductoParaBusqueda()
Set lnkProducto = objPageLogInLaComer.Link("html tag:=A", "name:="&strProductoParaBusqueda, "index:=0")
'''''''''''''''''''''''''''''''''''''''''''''Definition of Object names and Criticity''''''''''''''''''''''''''''''''''''''''''''
txtBusquedaDeProductos.SetTOProperty STR_NAME, "Busqueda de Productos"
txtBusquedaDeProductos.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
lnkProducto.SetTOProperty STR_NAME, "Buscando el producto: " &strProductoParaBusqueda
lnkProducto.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
bttnAgregarAlCarrito.SetTOProperty STR_NAME, "Agregar a Carrito"
bttnAgregarAlCarrito.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
imgCarrito.SetTOProperty STR_NAME, "Carrito"
imgCarrito.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
txtPzaCarrito.SetTOProperty STR_NAME, "Pza"
txtPzaCarrito.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
bttnX.SetTOProperty STR_NAME, "X"
bttnX.SetTOProperty oCriticity.CRITICITY, oCriticity.LOW
bttnEntregaAlDomicilio.SetTOProperty STR_NAME, "Entrega a Domicilio"
bttnEntregaAlDomicilio.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
bttnRecogerEnTienda.SetTOProperty STR_NAME, "Recoger en Tienda"
bttnRecogerEnTienda.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
imgArticulo1.SetTOProperty STR_NAME, "1er articulo en la lista"
imgArticulo1.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
imgArticuloCarrito.SetTOProperty STR_NAME, "Articulo en el carrito"
imgArticuloCarrito.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
bttnContinuar.SetTOProperty STR_NAME, "Continuar"
bttnContinuar.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
wbeGratis.SetTOProperty STR_NAME, "GRATIS"
wbeGratis.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
bttnAceptar.SetTOProperty STR_NAME, "Aceptar"
bttnAceptar.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
wbeAmex.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
wbeAmex.SetTOProperty STR_NAME, "amex"
imgLogo.SetTOProperty STR_NAME, "Logo La Comer"
imgLogo.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
wbeEliminarDelCarrito.SetTOProperty STR_NAME, "Eliminar del carrito"
wbeEliminarDelCarrito.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
'''''''''''''''''''''''''''''''''''''''''''''''''''''''Component Start''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Select Case strTestCaseId
	Case oTestCaseID.CP002
		strTestCaseName = oTestCaseName.CP002
	Case oTestCaseID.CP003
		strTestCaseName = oTestCaseName.CP003
	Case oTestCaseID.CP004
		strTestCaseName = oTestCaseName.CP004
	Case oTestCaseID.CP005
		strTestCaseName = oTestCaseName.CP005
	Case oTestCaseID.CP006
		strTestCaseName = oTestCaseName.CP006
End Select

'oReport.initReport objBrowserLogInLaComer
'oReport.initTestCaseLog "Unit Test"
oReport.initComponentLog strTestCaseName



txtBusquedaDeProductos.Set strProductoParaBusqueda
bttnBusqueda.Click
wait 4

If strTestCaseId = oTestCaseID.CP002 or strTestCaseId = oTestCaseID.CP003 or strTestCaseId = oTestCaseID.CP004 or strTestCaseId = oTestCaseID.CP005 or strTestCaseId = oTestCaseID.CP006 Then

	If lnkProducto.Exist(20) Then
		oReport.addPassStep "Se encontraron resultados relacionado con el producto de búsqueda: " &strProductoParaBusqueda, "", True
	Else
		oReport.addFailStep "No se encontraron resultados relacionados con el producto de búsqueda: " &strProductoParaBusqueda, "", True
	End If
	
End If		
		
If strTestCaseId = oTestCaseID.CP003 or strTestCaseId = oTestCaseID.CP004 or strTestCaseId = oTestCaseID.CP005 or strTestCaseId = oTestCaseID.CP006 Then
	'fnOnMouseMove(lnkProducto)
	strAltArticulo = fnGetAlt(imgArticulo1)
	strAltArticulo1 = Split(strAltArticulo, "-")(2)
	imgArticulo1.SetTOProperty STR_NAME, "Nombre del 1er articulo: " & strAltArticulo1
	wait 1
	bttnAgregarAlCarrito.Click
	imgCarrito.Click
	wait 1
	strPzaCarrito = fnGetValue(txtPzaCarrito)
	If txtPzaCarrito.Exist(2) and strPzaCarrito => 1 Then
		oReport.addPassStep "Producto agregado correctamente al carrito", "", True
	Else
		oReport.addFailStep "El Producto no fue agregado correctamente al carrito", "", True
	End If
	
	If strTestCaseId = oTestCaseID.CP003 Then
		wbeEliminarDelCarrito.Click
		wait 1
		bttnX.Click
	End If
End If

If strTestCaseId = oTestCaseID.CP004 or strTestCaseId = oTestCaseID.CP005 or strTestCaseId = oTestCaseID.CP006 Then
	bttnRecogerEnTienda.Click
	wait 1
	txtPzaCarrito.RefreshObject()
	strPzaCarrito = fnGetValue(txtPzaCarrito)
	strAltArticuloCarrito1 = fnGetAlt(imgArticuloCarrito)
	strAltArticuloCarrito = Split(strAltArticuloCarrito1, "-")(2)
	If txtPzaCarrito.Exist(2) and strPzaCarrito => 1 and strAltArticulo1 = strAltArticuloCarrito Then
		oReport.addPassStep "Producto agregado correctamente al carrito", "", True
	Else
		oReport.addFailStep "El Producto no fue agregado correctamente al carrito", "", True
	End If
End If

If strTestCaseId = oTestCaseID.CP005 or strTestCaseId = oTestCaseID.CP006 Then
	bttnContinuar.Click
	If wbeGratis.Exist(20) Then
		oReport.addPassStep "Se muestran dias y horarios para recoger el pedido", "", True
	Else
		oReport.addFailStep "No se muestran dias y horarios para recoger el pedido", "", True
	End If

End IF

If strTestCaseId = oTestCaseID.CP006 Then
	wbeGratis.Click
	bttnAceptar.Click
	wait 1
	wbeEnLinea.Click
	If wbeAmex.Exist(15) Then
		oReport.addPassStep "Se muestran las opciones de pago", "", True
	Else
		oReport.addFailStep "No se muestran las opciones de pago", "", True
	End If
End IF

'Para eliminar los productos del carrito con cada iteracion
If strTestCaseId = oTestCaseID.CP004 or strTestCaseId = oTestCaseID.CP005 or strTestCaseId = oTestCaseID.CP006 Then
	imgLogo.Click
	wait 7
	imgCarrito.Click
	wait 1
	wbeEliminarDelCarrito.Click
End IF

oReport.closeComponentLog()
'oReport.closeTestCaseLog()
