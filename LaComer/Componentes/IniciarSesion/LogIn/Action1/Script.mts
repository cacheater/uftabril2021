﻿'Component Description:
'Login into 'La Comer' application
'Preconditions: To have Email and Password
'Tester: Fabian Solano Amador
'Creation Date: 24-February-2020
'http://lacomer.com.mx/lacomer/doPresentaLogin.action?succFmt=100&succld=148&key=LaComer-Lomas-An%C3%A1huac&pago=

''''''''''''''''''''''''''''''''''''''''''''''Object Repository Declaration''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Set objBrowserLogInLaComer = Browser("name:=La Comer.*")
'Set objPageLogInLaComer = objBrowserLogInLaComer.Page("title:=La Comer.*")

'Set objBrowserLogInLaComer = Browser("name:=La Comer | Login")
'Set objPageLogInLaComer = objBrowserLogInLaComer.Page("title:=La Comer | Login")

Set objBrowserLogInLaComer = Browser("Browser")
Set objPageLogInLaComer = objBrowserLogInLaComer.Page("La Comer | Login")
objPageLogInLaComer.Highlight
Set txtEmail = objPageLogInLaComer.WebEdit("html id:=login-user")
Set txtContrasenia = objPageLogInLaComer.WebEdit("html id:=login-password")
Set wbeEntrar = objPageLogInLaComer.WebElement("html tag:=A", "innertext:=Entrar")

'Set wbeBienvenido = objPageLogInLaComer.WebElement("html tag:=SPAN", "innertext:=Bienvenido(a).*", "class:=mensaje_envio_green", "index:=0")
Set bttnComprarAhora = objPageLogInLaComer.WebButton("name:=COMPRA AHORA")

Set wbeHolaUsuario = objPageLogInLaComer.WebElement("html id:=helolbl")

''''''''''''''''''''''''''''''''''''''''''''''Global Variables Declaration'''''''''''''''''''''''''''''''''''''''''''''''''''''''
Const STR_NAME = "objName"
Const STR_NOMBRE_DEL_COMPONENTE = "Iniciar Sesion"
strEmail = fnGetEmail()
strContrasenia = fnGetContrasenia()
strUrl = fnGetUrl()
strBrowser = fnGetNavegador()
'''''''''''''''''''''''''''''''''''''''''''''Definition of Object names and Criticity''''''''''''''''''''''''''''''''''''''''''''
txtEmail.SetTOProperty STR_NAME, "Email"
txtEmail.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
txtContrasenia.SetTOProperty STR_NAME, "Contrasenia"
txtContrasenia.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
wbeEntrar.SetTOProperty STR_NAME, "Entrar"
wbeEntrar.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
bttnComprarAhora.SetTOProperty STR_NAME, "Comprar Ahora"
bttnComprarAhora.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
wbeHolaUsuario.SetTOProperty STR_NAME, "Hola Usuario"
wbeHolaUsuario.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
'''''''''''''''''''''''''''''''''''''''''''''''''''''''Component Start''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'fnOpenBrowser strUrl, strBrowser

'Device("Device").App("Home").MobileLabel("Aplicaciones").Tap
'wait 1
'Device("Device").App("Home").MobileLabel("Chrome").Tap
'wait 1
'Browser("Browser").Navigate strUrl

oReport.initReport objBrowserLogInLaComer
oReport.initTestCaseLog "Unit Test"
oReport.initComponentLog STR_NOMBRE_DEL_COMPONENTE

txtEmail.Set strEmail
wait 2
txtContrasenia.Set strContrasenia
wbeEntrar.Click

If bttnComprarAhora.Exist(7) Then
	oReport.addPassStep "Se inicio sesión correctamente", "", True
Else
	oReport.addFailStep "No fue posible iniciar sesion", "", True
End If

If bttnComprarAhora.Exist(1) Then
	bttnComprarAhora.Click
End If

If wbeHolaUsuario.Exist(12) Then
	strUsuario1 = fnGetInnertext(wbeHolaUsuario)
	strUsuario = Trim(Split(strUsuario1, ":")(1))
	oReport.addPassStep "Se inicio sesión correctamente con el usuario: " &strUsuario, "", True
Else
	oReport.addFailStep "No fue posible iniciar sesion", "", True
End If



oReport.closeComponentLog()
oReport.closeTestCaseLog()
