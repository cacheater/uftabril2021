﻿'Component Description:
'Logout into 'La Comer' application
'Preconditions: To have an account initialized
'Tester: Fabian Solano Amador
'Creation Date: 25-February-2020

''''''''''''''''''''''''''''''''''''''''''''''Object Repository Declaration''''''''''''''''''''''''''''''''''''''''''''''''''''''
Set objBrowserLogInLaComer = Browser("name:=La Comer.*")
Set objPageLogInLaComer = objBrowserLogInLaComer.Page("title:=La Comer.*")

Set lnkSalir = objPageLogInLaComer.Link("html tag:=A", "name:=Salir", "index:=0")
Set bttnSalir = objPageLogInLaComer.WebButton("html tag:=INPUT", "name:=SALIR")

Set lnkIngresa = objPageLogInLaComer.Link("html id:=lnkLogin")
''''''''''''''''''''''''''''''''''''''''''''''Global Variables Declaration'''''''''''''''''''''''''''''''''''''''''''''''''''''''
Const STR_NAME = "objName"
Const STR_NOMBRE_DEL_COMPONENTE = "Cerrar Sesion"
'''''''''''''''''''''''''''''''''''''''''''''Definition of Object names and Criticity''''''''''''''''''''''''''''''''''''''''''''
lnkSalir.SetTOProperty STR_NAME, "Salir"
lnkSalir.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
bttnSalir.SetTOProperty STR_NAME, "Salir"
bttnSalir.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
lnkIngresa.SetTOProperty STR_NAME, "Ingresa"
lnkIngresa.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
'''''''''''''''''''''''''''''''''''''''''''''''''''''''Component Start''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'oReport.initReport objBrowserLogInLaComer
'oReport.initTestCaseLog "Unit Test"
oReport.initComponentLog STR_NOMBRE_DEL_COMPONENTE


lnkSalir.Click
wait 1
bttnSalir.Click

'Si existe el link ingresa, el caso de prueba pasa
If lnkIngresa.Exist(12) Then
	oReport.addPassStep "Se cerró sesión correctamente", "", True
	fnCloseBrowser()
Else
	oReport.addFailStep "No fue posible cerrar la sesión", "", True
End If



oReport.closeComponentLog()
'oReport.closeTestCaseLog()
