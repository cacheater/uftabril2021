﻿'Objetos del repositorio
Set objBrowser = Browser("name:=Login - My Store")
Set objPage = objBrowser.Page("title:=Login - My Store")

Set objBrowserMyAccount = Browser("name:=My account - My Store")
Set objPageMyAccount = objBrowserMyAccount.Page("title:=My account - My Store")

Set objBrowserEvening = Browser("name:=Evening Dresses - My Store")
Set objPageEvening = objBrowserEvening.Page("title:=Evening Dresses - My Store")

Set editEmail = objPage.WebEdit("html id:=email")
Set editPasswd = objPage.WebEdit("html id:=passwd")
Set buttonSignIn = objPage.WebButton("html id:=SubmitLogin")
'Set linkDresses = objPageMyAccount.Link("html tag:=A", "name:=Dresses", "class:=sf-with-ul", "index:=1")
Set linkDresses = objPageMyAccount.Link("xpath:=(//a[@title='Dresses'])[2]")
Set linkEveningDresses = objPageMyAccount.Link("html tag:=A", "name:=Evening Dresses", "index:=1")
Set objBrowserRepo = Browser("Evening Dresses - My Store")
Set objRepo = objBrowserRepo.Page("Evening Dresses - My Store")
Set AddToCart = objRepo.Link("Add to cart")
Set ProcedToCheckOut = objRepo.Link("Proceed to checkout")
Set linkSignOut = objRepo.Link("name:=Sign out", "index:=0")


'Variables Globales
strUsuario = DataTable.Value("USUARIO")
strPassword = DataTable.Value("PASSWORD")
strNavegador = DataTable.Value("NAVEGADOR")
URL = "http://automationpractice.com/index.php?controller=authentication&back=my-account"
Const OBJ_NAME = "objName"

'Criticidad
editEmail.SetTOProperty OBJ_NAME, "Email"
editEmail.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
editPasswd.SetTOProperty OBJ_NAME, "Password"
editPasswd.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
buttonSignIn.SetTOProperty OBJ_NAME, "SigIn"
buttonSignIn.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
linkDresses.SetTOProperty OBJ_NAME, "Desses"
linkDresses.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
linkEveningDresses.SetTOProperty OBJ_NAME, "EveningDresses"
linkEveningDresses.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
AddToCart.SetTOProperty OBJ_NAME, "Add to Cart"
AddToCart.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
ProcedToCheckOut.SetTOProperty OBJ_NAME, "Proceed to Checkout"
ProcedToCheckOut.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH
linkSignOut.SetTOProperty OBJ_NAME, "SignOut"
linkSignOut.SetTOProperty oCriticity.CRITICITY, oCriticity.HIGH

oReport.initReport(objPage)
oReport.initTestCaseLog("TC_SCF_001, Validar el ingreso de manera correcta al checkout")
oReport.initComponentLog("Componentes")

'Flujo del script
fnAbrirNavegador strNavegador, URL
wait 2

editEmail.Set strUsuario
editPasswd.Set strPassword
buttonSignIn.Click
wait 4
oReport.addPassStep "Inicio de sesion correcto", "", true

linkDresses.HoverTap
linkEveningDresses.Click
wait 4
oReport.addPassStep "Ingreso a la seccion Evening Dresses", "", true

AddToCart.Click
ProcedToCheckOut.Click
wait 5
oReport.addPassStep "Ingreso correcto al Checkout de la aplicacion", "", true

linkSignOut.Click
wait 5
oReport.addPassStep "Se cierra la sesion de manera correcta", "", true
objBrowserRepo.CloseAllTabs()


oReport.closeComponentLog()
oReport.closeTestCaseLog()
