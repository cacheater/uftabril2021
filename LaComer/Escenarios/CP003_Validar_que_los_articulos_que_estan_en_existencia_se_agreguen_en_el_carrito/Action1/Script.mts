﻿'Component Description:
'Realizan la busqueda de un articulo en la lupa
'Preconditions: 
'Tester: Fabian Solano Amador
'Creation Date: 25-February-2020
'''''''''''''''''''''''''''''''''''''''''''''''''''''''Scenario Start''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
fnImportData(oTestCaseID.CP003)
strEjecucion = fnGetEjecucion()


If strEjecucion = oEjecucion.SI Then
	
	Set objBrowserLogInLaComer = Browser("name:=La Comer.*")
	Set objPageLogInLaComer = objBrowserLogInLaComer.Page("title:=La Comer.*")
	
	oReport.initReport objBrowserLogInLaComer
	oReport.initTestCaseLog oTestCaseName.CP003
	
	
	RunAction "LogIn_La_Comer [LogIn]", oneIteration
	
	RunAction "Compra_De_Articulos [CompraDeArticulos]", oneIteration, oTestCaseID.CP003
	
	RunAction "LogOut_La_Comer [LogOut]", oneIteration
	
	
	oReport.closeTestCaseLog()
	ExitTest()
	
End If
