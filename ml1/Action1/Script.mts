﻿strData = DataTable.Value("A", dtGlobalSheet)
strDataLocal = DataTable.Value("B", dtLocalSheet)
DataTable.SetNextRow()
strSuma = DataTable.Value("B", dtGlobalSheet)

'DataTable.AddSheet("celulares")
strHojaExcel = "celulares"
strHojaUFT = "global"
DataTable.ImportSheet  "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\DATA\datos_para_validar.xlsx", strHojaExcel, strHojaUFT
wait 1

DataTable.SetNextRow()
DataTable.Value("C", dtLocalSheet) = "Diego"
DataTable.SetNextRow()
DataTable.Value("C", dtLocalSheet) = "Julio"
DataTable.SetNextRow()
DataTable.Value("C", dtLocalSheet) = "Fabian"
DataTable.Export "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\DATA\TablaEnTiempoEjecucion.xls"
wait 1

'precondicion: que exista al menos una publicacion retorna un True si existe una publicaccion para eliminar o False si no existe y ademas la crea
'Eliminar publicacion(True)











'Declaracion de objetos del repositorio
Set objBrowserML = Browser("Mercado Libre México")
Set objPageML = objBrowserML.Page("Mercado Libre México")

'Set objBrowserBusqueda = Browser("name:=Iphone | MercadoLibre.com.mx")
'Set objPageBusqueda = objBrowserBusqueda.Page("title:=Iphone | MercadoLibre.com.mx")

Set editBusqueda = objPageML.WebEdit("acc_name:=Ingresa lo que quieras encontrar")
Set elementBuscar = objPageML.WebElement("acc_name:=Buscar")

Set elementCambioVista = objPageML.WebElement("class:=ui-search-gallery-desktop", "html tag:=svg", "index:=0")
Set elementTitulo = objPageML.WebElement("html tag:=H2", "class:=ui-search-item__title ui-search-item__group__element", "index:=0")

Set objPageBusquedas = objBrowserML.Page("BusquedaGenerica")

'Declaracion de variables globales
DataTable.AddSheet("celulares")
DataTable.AddSheet("relojes")
DataTable.AddSheet("carteras")
DataTable.AddSheet("producto")
DataTable.ImportSheet  "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\DATA\datos_para_validar.xlsx", "celulares", "celulares"
DataTable.ImportSheet  "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\DATA\datos_para_validar.xlsx", "relojes", "relojes"
DataTable.ImportSheet  "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\DATA\datos_para_validar.xlsx", "carteras", "carteras"
DataTable.ImportSheet  "\\Mac\Home\Documents\Unified Functional Testing\UFTAbril2021\DATA\datos_para_validar.xlsx", "producto", "producto"
strNavegador = "chrome"
url = "https://www.mercadolibre.com.mx/"
'strBusqueda = "iphone"

For IteratorInicio = 1 To 3 Step 1
	
	If IteratorInicio = 1 Then
		strBusqueda = "iphone"
	ElseIf IteratorInicio = 2 Then
		strBusqueda = "relojes"
	Else
		strBusqueda = "carteras"
	End If
	
	'Flujo del script
	fnAbrirNavegador strNavegador, url
	
	editBusqueda.RefreshObject()
	editBusqueda.Set strBusqueda
	elementBuscar.RefreshObject()
	elementBuscar.Click
	wait 5
	elementCambioVista.Click
	print(strBusqueda)
	
	For Iterator = 0 To 19 Step 1
		Set elemento1 = objPageBusquedas.WebElement("html tag:=H2", "class:=ui-search-item__title ui-search-item__group__element", "index:="&Iterator)
		elemento1.Highlight
		strTituloActual = elemento1.GetROProperty("innertext")
		strTituloEsperado = DataTable.Value( "VALIDACIONES", "celulares")
		If strTituloActual = strTituloEsperado Then
			print("El titulo actual hace match con el titulo esperado: " &strTituloActual)
		Else
			print("El titulo actual NO hace match con el titulo esperado: " &strTituloEsperado& " Titulo Actual: " &strTituloActual)
		End If
		DataTable.SetNextRow()
	Next
	print("")
	print("")
objBrowserML.Close()
Next
