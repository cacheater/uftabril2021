﻿'Objetos del repositorio
Set objBrowser = Browser("name:=Login - My Store")
Set objPage = objBrowser.Page("title:=Login - My Store")

Set objBrowserMyAccount = Browser("name:=My account - My Store")
Set objPageMyAccount = objBrowserMyAccount.Page("title:=My account - My Store")

Set objBrowserEvening = Browser("name:=Evening Dresses - My Store")
Set objPageEvening = objBrowserEvening.Page("title:=Evening Dresses - My Store")

Set editEmail = objPage.WebEdit("html id:=email")
Set editPasswd = objPage.WebEdit("html id:=passwd")
Set buttonSignIn = objPage.WebButton("html id:=SubmitLogin")
'Set linkDresses = objPageMyAccount.Link("html tag:=A", "name:=Dresses", "class:=sf-with-ul", "index:=1")
Set linkDresses = objPageMyAccount.Link("xpath:=(//a[@title='Dresses'])[2]")
Set linkEveningDresses = objPageMyAccount.Link("html tag:=A", "name:=Evening Dresses", "index:=1")
Set linkSignOut = objPageEvening.Link("name:=Sign out", "index:=0")

'Variables Globales
strUsuario = DataTable.Value("USUARIO")
strPassword = DataTable.Value("PASSWORD")
strNavegador = DataTable.Value("NAVEGADOR")
Const OBJ_NAME = "objName"
Const CRITICITY = "High"


'Criticidad
editEmail.SetTOProperty OBJ_NAME, "Email"
editEmail.SetTOProperty CRITICITY, "High"

'Flujo del script
SystemUtil.CloseProcessByName "chrome.exe"
SystemUtil.CloseProcessByName "firefox.exe"
SystemUtil.CloseProcessByName "iexplore.exe"
SystemUtil.CloseProcessByName "msedge.exe"
wait 1

fnAbrirNavegador "chrome", "http://automationpractice.com/index.php?controller=authentication&back=my-account"
wait 2

editEmail.Set strUsuario
editPasswd.Set strPassword @@ script infofile_;_ZIP::ssf2.xml_;_
buttonSignIn.Click @@ script infofile_;_ZIP::ssf3.xml_;_
wait 4
linkDresses.HoverTap
linkEveningDresses.Click
wait 4
linkSignOut.Click
objBrowserEvening.CloseAllTabs()
