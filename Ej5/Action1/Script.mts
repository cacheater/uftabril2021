﻿'Cree una estructura If que evalúe 2 variables; a = 20, b = 90. 
'Verifique que las variables a y b sean mayor o igual que 10 y después sume sus valores.

Dim a, b, suma
a=20
b=90

If (a>=10) and (b>=10) then
	suma=a+b
	print (suma)
End If
