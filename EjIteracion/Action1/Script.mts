﻿'Realizar un programa que pida como entrada un mensaje y se itere hasta que el usuario introduzca la palabra terminar

strPalabraParaTerminarPrograma = "Terminar"
strMensajeEntrada = inputbox("Escribe un mensaje")

While strMensajeEntrada <> strPalabraParaTerminarPrograma
	MsgBox(strMensajeEntrada)
	strMensajeEntrada = inputbox("Escribe un mensaje")
Wend

MsgBox("El programa ha terminado")
